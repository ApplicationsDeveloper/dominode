/**
 * Game Engine's toolbox
 */

"use strict";

class ToolBox {
	constructor() {}

	/* Single-unit Methods */

	async log(contentStr) {
		
		try { return await process.stdout.write(contentStr + "\n"); }
		
		catch(error) { console.error(error); }
	}

	async consoleBeautify(string) {

		try {
			let outputStr = string;
			
			outputStr = await outputStr.replace(/&#33;/gi, "!");
			outputStr = await outputStr.replace(/&#39;/gi, "'");
			outputStr = await outputStr.replace(/&#40;/gi, "(");
			outputStr = await outputStr.replace(/&#41;/gi, ")");
			outputStr = await outputStr.replace(/&#44;/gi, ",");
			outputStr = await outputStr.replace(/&#58;/gi, ":");
			outputStr = await outputStr.replace(/&#60;/gi, "<");
			outputStr = await outputStr.replace(/&#62;/gi, ">");

			return outputStr;
		}

		catch(error) { console.error(error); }
	}

	async shuffleArray(array) {

		try {
			return await array.sort((a, b) => { return 0.5 - Math.random(); }); // array
		}

		catch(error) { console.error(error); }
	}

	async getRandomIndexFromArray(array) {

		try {
			return await Math.floor(Math.random() * array.length); // integer
		}
		
		catch(error) { console.error(error); }
	}

	async getTile(array, indexInt) {

		try {
			return await array[indexInt]; // any data type
		}
		
		catch(error) { console.error(error); }
	}

	async removeTile(array, indexInt) {

		try {
			let removedTile = await array.splice(indexInt, 1);
			return array;
		}
		
		catch(error) { console.error(error); }
	}

	async whoPlaysFirst(playersArr) {

		try {
			return await this.getRandomIndexFromArray(playersArr); // integer
		}

		catch(error) { console.error(error); }
	}

	async whoPlaysNext(playersArr, currentPlayerIndexInt) {

		try {
			if (currentPlayerIndexInt === (playersArr.length - 1)) { return 0; }
			else { return currentPlayerIndexInt + 1; }
		}

		catch(error) { console.error(error); }
	}

	async playerExaminesPossibleOptions(deckArr, boardArr) {

		try {
			let possibleTilesToChooseFromDeckObj = {
				"tKey": [],
				"tValue": []
			};

			for (let x = 0; x < deckArr.length; x++) {

				for (let y = 0; y < deckArr[x].length; y++) {

					if (
						(deckArr[x][y] === boardArr[0][0]) ||
						(deckArr[x][y] === boardArr[boardArr.length-1][boardArr[boardArr.length-1].length-1])
					) {
						await possibleTilesToChooseFromDeckObj.tKey.push(x);
						await possibleTilesToChooseFromDeckObj.tValue.push(deckArr[x]);
					}
				}
			}

			return possibleTilesToChooseFromDeckObj;
		}

		catch(error) { console.error(error); }
	}

	async boardStatusStr(msgStr, array, startStr, delimiterStr, endStr, nbspStr) {

		try {
			let outputStr = "";
			let tempStr = "";
			
			for (let x = 0; x < array.length; x++) {

				tempStr += startStr;

				for (let y = 0; y < array[x].length; y++) {

					tempStr += array[x][y].toString();
					
					if (y < (array[x].length - 1)) { tempStr += delimiterStr; }
				}
				
				tempStr += endStr;
				outputStr += tempStr;
				if (x < (array.length - 1)) { outputStr += nbspStr; }

				// Return to default
				tempStr = "";
			}
			return await msgStr + nbspStr + outputStr;
		}

		catch(error) { console.error(error); }
	}

	async playerStatusStr(
		playerNameStr,
		playerMsgObj,
		tileDrawStatusBool,
		drawTileArr,
		playTileArr,
		connectToBoardTileArr,
		tileReverseStatusBool,
		startStr,
		delimiterStr,
		endStr,
		nbspStr,
		tileSquareUpKeyInt,
		tileSquareDownKeyInt
	) {
		try {
			if (tileDrawStatusBool === true) {

				return await playerNameStr + nbspStr + playerMsgObj.Draws + nbspStr
				+ startStr + drawTileArr[tileSquareUpKeyInt] + delimiterStr
				+ drawTileArr[tileSquareDownKeyInt] + endStr;
			}
			else {
				if (tileReverseStatusBool === false) {

					return await playerNameStr + nbspStr + playerMsgObj.Plays.Start + nbspStr
					+ startStr + playTileArr[tileSquareUpKeyInt] + delimiterStr
					+ playTileArr[tileSquareDownKeyInt] + endStr + nbspStr
					+ playerMsgObj.Plays.Where + nbspStr + startStr
					+ connectToBoardTileArr[tileSquareUpKeyInt] + delimiterStr
					+ connectToBoardTileArr[tileSquareDownKeyInt] + endStr;
				}
				else {

					return await playerNameStr + nbspStr + playerMsgObj.Plays.Start + nbspStr
					+ startStr + playTileArr[tileSquareUpKeyInt] + delimiterStr
					+ playTileArr[tileSquareDownKeyInt] + endStr + nbspStr
					+ playerMsgObj.Reverse + nbspStr + playerMsgObj.Plays.Where + nbspStr
					+ startStr + connectToBoardTileArr[tileSquareUpKeyInt] + delimiterStr
					+ connectToBoardTileArr[tileSquareDownKeyInt] + endStr;
				}
			}
		}

		catch(error) { console.error(error); }
	}

	async winStatusStr(envObjStatusWinner, playerNameStr, nbspStr) {
		try {
			return await envObjStatusWinner.Start + nbspStr + playerNameStr + nbspStr + envObjStatusWinner.End;
		}

		catch(error) { console.error(error); }
	}

	/* Wrappers or Complex Methods */

	async drawOneRandomTile(array) {

		try {
			let index = await this.getRandomIndexFromArray(array);
			let output = {
				"tile": await this.getTile(array, index),
				"stock": await this.removeTile(array, index)
			};
			return output;
		}

		catch(error) { console.error(error); }
	}

	async drawSevenRandomTiles(array) {

		try {
			let tempObject = {};
			let tempArray = array;
			
			let output = {
				"deck": [],
				"stock": []
			};

			for (let i = 0; i < 7; i++) {
				tempObject = await this.drawOneRandomTile(tempArray);
				await output.deck.push(tempObject.tile);
				tempArray = tempObject.stock;
			}

			// the last updated stock after drawing 7 tiles
			output.stock = tempArray;

			return output;
		}

		catch(error) { console.error(error); }
	}

	async playerMakesDecision(
		possibleTilesToPlayObj,
		deckArr,
		stockArr,
		boardArr,
		squareUpKeyInt,
		squareDownKeyInt
	) {
		try {
			let output = {
				"deck": deckArr,
				"stock": stockArr,
				"board": boardArr,
				"tileDrawStatus": false,
				"drawTile": [],
				"playTile": [],
				"connectToBoardTile": [],
				"tileReverseStatus": false
			};
			let tempObject = {};
			let playNowStatus = false;

			// Player does not have matching tile so he will draw new tile from the stock
			if (possibleTilesToPlayObj.tValue.length === 0) {
				
				tempObject = await this.drawOneRandomTile(stockArr);

				// Update output object, where needed
				await output.deck.push(tempObject.tile);
				output.stock = tempObject.stock;
				output.tileDrawStatus = true;
				output.drawTile = tempObject.tile;
			}

			// Player has one or more matching tiles in his deck,
			// so he will decide which tile to play & where to put it on the board
			else {
				let tempIndex = await this.getRandomIndexFromArray(possibleTilesToPlayObj.tValue);
				
				// Mapping for easier code readability
				let chooseOneTileToPlayArr =  await this.getTile(possibleTilesToPlayObj.tValue, tempIndex);
				let chooseOneTileToPlayIndexOnDeck = possibleTilesToPlayObj.tKey[tempIndex];

				// Update output object, where needed
				output.playTile = chooseOneTileToPlayArr.slice(0);

				// Mapping for easier code readability
				let boardLeftEdgeTileArr = boardArr[0];
				let boardLeftEdgeTileArrFirstValueInt = boardArr[0][0];
				let boardRightEdgeTileArr = boardArr[boardArr.length-1];
				let boardRightEdgeTileArrLastValueInt = boardArr[boardArr.length-1][boardArr[boardArr.length-1].length-1];

				/* Non Tile-reversing decisions */
				// Tile matching at the start of the board
				if ((playNowStatus === false) && (chooseOneTileToPlayArr[squareDownKeyInt] === boardLeftEdgeTileArrFirstValueInt)) {
					
					playNowStatus = true;

					// Update output object, where needed
					await output.board.unshift(chooseOneTileToPlayArr);
					output.deck = await this.removeTile(deckArr, chooseOneTileToPlayIndexOnDeck);
					output.connectToBoardTile = boardLeftEdgeTileArr;
				}
				// Tile matching at the end of the board
				if ((playNowStatus === false) && (chooseOneTileToPlayArr[squareUpKeyInt] === boardRightEdgeTileArrLastValueInt)) {

					playNowStatus = true;

					// Update output object, where needed
					await output.board.push(chooseOneTileToPlayArr);
					output.deck = await this.removeTile(deckArr, chooseOneTileToPlayIndexOnDeck);
					output.connectToBoardTile = boardRightEdgeTileArr;
				}
				
				/* Tile-reversing decisions */
				// Tile matching at the start of the board
				if ((playNowStatus === false) && (chooseOneTileToPlayArr[squareUpKeyInt] === boardLeftEdgeTileArrFirstValueInt)) {

					playNowStatus = true;
					chooseOneTileToPlayArr.reverse();

					// Update output object, where needed
					await output.board.unshift(chooseOneTileToPlayArr);
					output.deck = await this.removeTile(deckArr, chooseOneTileToPlayIndexOnDeck);
					output.connectToBoardTile = boardLeftEdgeTileArr;
					output.tileReverseStatus = true;
				}
				// Tile matching at the end of the board
				if ((playNowStatus === false) && (chooseOneTileToPlayArr[squareDownKeyInt] === boardRightEdgeTileArrLastValueInt)) {

					playNowStatus = true;
					chooseOneTileToPlayArr.reverse();

					// Update output object, where needed
					await output.board.push(chooseOneTileToPlayArr);
					output.deck = await this.removeTile(deckArr, chooseOneTileToPlayIndexOnDeck);
					output.connectToBoardTile = boardRightEdgeTileArr;
					output.tileReverseStatus = true;
				}
			}

			return output;
		}

		catch(error) { console.error(error); }
	}
}

module.exports = ToolBox;
