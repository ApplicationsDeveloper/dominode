/**
 * Stock generator
 * Creates the stock of game tiles
 */

"use strict";

class StockGenerator {
	
	constructor() {
		this._stockBucket = [];
	}

	getStockBucket() { return this._stockBucket; }

	async generate() {

		try {
			const loop = async () => {

				// Vertical development | height | x
				for (let x = 0; x < 7; x++) {

					// Horizontal development | length | y
					// x + 1 = quantity of tiles of current row
					for (let y = 0; y < (x + 1); y++) {

						// y = "up" square | x = "down" square
						await this._stockBucket.push([y,x]);
					}
				}
				return;
			};
			
			await loop();
			return;
		}
		
		catch(error) { console.error(error); }
	}
}

module.exports = StockGenerator;
