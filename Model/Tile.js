/**
 * Data model for the entity: Tile
 */

"use strict";

function Tile() {

	this.Style = {
		"Start": "",
		"Delimiter": "",
		"End": ""
	};
	
	this.Square = {
		"Up": {
			"Key": null,
			"Value": null
		},
		"Down": {
			"Key": null,
			"Value": null
		}
	};
}

module.exports = Tile;
